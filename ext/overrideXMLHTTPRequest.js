
console.log("HELLO");

console.log(window);
console.log(this);

window.waitingAPICalls = 0;

console.log(window.waitingAPICalls);

const originalSend = XMLHttpRequest.prototype.send;

XMLHttpRequest.prototype.send = function () {
    window.waitingAPICalls++;
    console.log(window.waitingAPICalls);
    const _onreadystatechange = this.onreadystatechange;

    this.onreadystatechange = function () {
      if (this.readyState === 4) {
        window.waitingAPICalls--;
        console.log(waitingAPICalls);
      }
      if (_onreadystatechange) {
        _onreadystatechange.apply(this, arguments);
      }
    }
    originalSend.apply(this, arguments);
  }
  