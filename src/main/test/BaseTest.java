package main.test;

import main.java.*;
import main.java.annotations.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest extends CopperheadHookProvider {

	protected static WebDriver wd;
	
	public static void beforeProcess() {
		//wd = new ChromeDriver();
		//wd.get("");
		System.out.println("Starting process");
		//wd = new ChromeDriver();
		//wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	public static void afterProcess() {
		try {
			Thread.sleep(1000);
		} catch (Exception e) {}
		//wd.quit();
	}
	
	@Step("Open \"([a-zA-Z0-9:/\\-\\.]+)\"")
	public static void goToPage(String thing) {
		System.out.println("Going to page " + thing);
		wd.get(thing);
	}

	@Step("Click the \"([a-zA-Z0-9\\s\\-\\.]+)\" on the \"([a-zA-Z0-9\\s\\-\\.]+)\" page")
	public static void clickOnThePage(String element, String page) {
		By locator = PageIndexer.getLocatorFromElementAndPage(element,page);
		System.out.println("Going to click " + locator);
		WebElement wElem = wd.findElement(locator);
		((JavascriptExecutor) wd).executeScript("arguments[0].scrollIntoView(true);", wElem);
		try {
			Thread.sleep(500);
		} catch (Exception e) {}
		wElem.click();
	}

	@Step("Enter \"([^\"]*)\" into the \"([^\"]*)\" field on the \"([^\"]*)\" page")
	public static void enterIntoTheFieldOnThePage(String inputText, String element, String page) {
		By locator = PageIndexer.getLocatorFromElementAndPage(element,page);
		System.out.println("Going to enter text into " + locator);
		WebElement wElem = wd.findElement(locator);
		((JavascriptExecutor) wd).executeScript("arguments[0].scrollIntoView(true);", wElem);
		try {
			Thread.sleep(500);
		} catch (Exception e) {}
		wElem.sendKeys(inputText);
	}

	@Step("Clear the \"([^\"]*)\" field on the \"([^\"]*)\" page")
	public static void clearTheFieldOnThePage(String element, String page) {
		By locator = PageIndexer.getLocatorFromElementAndPage(element,page);
		System.out.println("Going to enter text in " + locator);
		WebElement wElem = wd.findElement(locator);
		((JavascriptExecutor) wd).executeScript("arguments[0].scrollIntoView(true);", wElem);
		try {
			Thread.sleep(500);
		} catch (Exception e) {}
		wElem.clear();
	}

	@Step("the text value of the \"([^\"]*)\" element on the \"([^\"]*)\" page")
	public static String textValueOfTheElementOnThePage(String element, String page) {
		By locator = PageIndexer.getLocatorFromElementAndPage(element,page);
		System.out.println("Getting the text for " + locator);
		WebElement wElem = wd.findElement(locator);
		((JavascriptExecutor) wd).executeScript("arguments[0].scrollIntoView(true);", wElem);
		try {
			Thread.sleep(500);
		} catch (Exception e) {}
		return wElem.getText();
	}

	@Step("Verify (.*) is (.*)")
	public static void verifyEquality(String val1, String val2) {
		System.out.println("Verifying equality: ");
		System.out.println(val1);
		System.out.println(val2);
		Object val1Result = StepProcessor.runStep(val1);
		Object val2Result = StepProcessor.runStep(val2);
		if (!val1Result.equals(val2Result)) {
			throw new RuntimeException("The values are not equal.");
		}
	}

	@Step("Given (.*)")
	public static void givenStep(String step) {
		String thenStep = "Then "+step;
		CopperheadFileParser.CopperheadProcess process = ProcessRunner.findProcessEndingInStep(thenStep);
		ProcessRunner.runNakedProcess(process);
	}

	@Step("Print (.*)")
	public static void printString(String text) {
		System.out.println(text);
	}

	@Step("\"([^\"]*)\"")
	public static String stringConstant(String val) {
		System.out.println("Getting string constant: "+val);
		return val;
	}
	
}
