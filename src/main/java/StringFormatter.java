package main.java;

public class StringFormatter {

    public static String regToCamelCase(String reg) {
        String[] tokens = reg.split("\\s");
        for (int i=1;i<tokens.length;i++) {
            tokens[i] = tokens[i].substring(0,1).toUpperCase() + tokens[i].substring(1);
        }
        return String.join("",tokens);
    }

    public static void main(String[] args) {
        System.out.println(regToCamelCase("apple banana cabbage"));
    }

}
