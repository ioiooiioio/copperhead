package main.java;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import main.java.CopperheadFileParser.CopperheadGroup;

public class DirectoryIterator {

	public static List<File> getFilesInDirectory(File directory, String fileRegex) {
		return getFilesInDirectory(directory,Pattern.compile(fileRegex));
	}
	
	public static List<File> getFilesInDirectory(File directory, Pattern fileRegex) {
		List<File> toReturn = new ArrayList<File>();
		List<File> subdirectories = new ArrayList<File>();
		subdirectories.add(directory);
		while (!subdirectories.isEmpty()) {
			File f = subdirectories.remove(0);
			if (f.isFile()) {
				toReturn.add(f);
			} else if (f.isDirectory()) {
				File[] subfiles = f.listFiles();
				for (File subfile:subfiles) {
					subdirectories.add(subfile);
				}
			}
		}
		return toReturn;
	}
	
}
