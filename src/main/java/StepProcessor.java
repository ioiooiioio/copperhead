package main.java;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.java.annotations.Step;

public class StepProcessor {

	private static String stepPackageName;
	
	private static List<Method> stepMethods = new ArrayList<Method>();

	public void init(String packageName) {
		stepPackageName = packageName;
		try {
			findAllClasses();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StepProcessor(String packageName) {
		this.stepPackageName = packageName;
		try {
			findAllClasses();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void findAllClasses() throws Exception {
		List<File> files = DirectoryIterator.getFilesInDirectory(new File(stepPackageName),".*.class");
		for (File file:files) {
			findClasses(file);
		}
	}
	
	private static void findClasses(File stepDef) throws ClassNotFoundException {
	    try {
	    	URL url = stepDef.toURL();
	    	URL[] urls = new URL[]{url};
	    	ClassLoader cl = new URLClassLoader(urls);
	    	Class cls = cl.loadClass(classPathFromFile(stepDef));
	    	loadMethodsFromClass(cls);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	}

	private static void loadMethodsFromClass(Class cls) {
		Method[] methods = cls.getDeclaredMethods();
		for (Method m:methods) {
			Step stepAnnotation = m.getAnnotation(Step.class);
			if (stepAnnotation != null) {
				String regex = stepAnnotation.value();
				stepMethods.add(m);
			}
		}
	}

	private static String classPathFromFile(File f) {
		String s = f.getPath();
		s = s.replaceAll("\\\\",".");
		s = s.substring(s.indexOf(".")+1);
		s = s.substring(0, s.lastIndexOf("."));
		return s;
	}
	
	public static Object runStep(String step) {
		for (int i=0;i<stepMethods.size();i++) {
			Method method = stepMethods.get(i);
			Pattern regex = Pattern.compile(method.getAnnotation(Step.class).value());
			Matcher regexMatcher = regex.matcher(step);
			if (regexMatcher.matches()) {
				int paramCount = regexMatcher.toMatchResult().groupCount();
				Object[] params = new Object[paramCount];
				for (int j=0;j<paramCount;j++) {
					params[j] = regexMatcher.toMatchResult().group(j+1);
				}
				try {
					return method.invoke(null, params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}
		}
		System.out.println("Step not found in java defs: "+step);
		for (CopperheadFileParser.CopperheadGroup copperheadGroup:ProcessRunner.groupLibrary) {
			for (CopperheadFileParser.CopperheadProcess copperheadProcess:copperheadGroup.getProcesses()) {
				if (copperheadProcess.getName().equalsIgnoreCase(step)) {
					ProcessRunner.runNakedProcess(copperheadProcess);
					return null;
				}
			}
		}
		System.out.println("Step not found in process library: "+step);
		return null;
	}
	
}
