package main.java;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CopperheadFileParser {

	public static CopperheadGroup parseCopperheadFile(File f) throws Exception {
		CopperheadGroup group = new CopperheadGroup();
		CopperheadProcess process = null;
		CopperheadTable table = null;
		List<String> cachedTags = new ArrayList<>();
		Scanner in = new Scanner(f);
		while (in.hasNext()) {
			String inLine = in.nextLine().trim();
			CopperheadFileLineType lineType = CopperheadFileLineType.identifyLine(inLine);
			switch (lineType) {
			case GROUP_DEF: 
				group.name = inLine; 
				group.tags = cachedTags;
				cachedTags = new ArrayList<>();
				break;
			case PROCESS_DEF: 
				if (process!=null) {
					group.processes.add(process);
				}
				process = new CopperheadProcess();
				process.name = inLine.substring(inLine.indexOf(":")+1).trim();
				process.tags = cachedTags;
				cachedTags = new ArrayList<>();
				break;
			case TABLE_DEF:
				if (table!=null) {
					group.tables.add(table);
				}
				table = new CopperheadTable();
				table.name = inLine.substring(inLine.indexOf(":")+1).trim();
				break;
			case PARAM_DEF:
				process.paramSource = inLine.substring(inLine.indexOf(":")+1).trim();
				break;
			case TABLE_ENTRY:
				List<String> rowItems = Arrays.asList(inLine.split("\\|"));
				for (int i=0;i<rowItems.size();i++) {
					rowItems.set(i,rowItems.get(i).trim());
				}
				if (table.hasHeaders()) {
					table.dataValues.add(rowItems);
				} else {
					table.setHeaders(rowItems);
				}
				break;
			case TAGS: 
				cachedTags.add(inLine);
				break;
			case COMMENT:
				break;
			case BLANK: break;
			case STEP: process.steps.add(inLine); break;
			}
		}
		if (process!=null) {
			group.processes.add(process);
		}
		if (table!=null) {
			group.tables.add(table);
		}
		return group;
	}
	
	public static class CopperheadGroup {
		
		List<String> tags = new ArrayList<>();
		String name;
		List<CopperheadProcess> processes = new ArrayList<>();
		List<CopperheadTable> tables = new ArrayList<>();
		
		public String toString() {
			String toReturn = this.name+'\n';
			toReturn+=tags.toString()+'\n';
			toReturn+="Processes: \n";
			for (CopperheadProcess process:processes) {
				toReturn+=process.toString()+"\n\n";
			}
			toReturn+="\n";
			toReturn+="Tables: \n";
			for (CopperheadTable table:tables) {
				toReturn+=table.toString()+"\n\n";
			}
			return toReturn;
		}
		
		public List<String> getTags() {
			return tags;
		}
		
		public String getName() {
			return name;
		}
		
		public List<CopperheadProcess> getProcesses() {
			return processes;
		}

		public List<CopperheadTable> getTables() {
			return tables;
		}
		
	}
	
	public static class CopperheadProcess {
		
		List<String> tags = new ArrayList<>();
		String name;
		List<String> steps = new ArrayList<>();
		String paramSource = null;
		
		public String toString() {
			String toReturn = this.name+'\n';
			toReturn+=tags.toString()+'\n';
			for (String step:steps) {
				toReturn+=step+'\n';
			}
			return toReturn;
		}

		public boolean hasParamSource() {
			return paramSource!=null;
		}

		public String getParamSource() {
			return paramSource;
		}
		
		public List<String> getTags() {
			return tags;
		}
		
		public String getName() {
			return name;
		}
		
		public List<String> getSteps() {
			return steps;
		}
		
	}

	public static class CopperheadTable {

		String name;
		List<String> headers = null;
		List<List<String>> dataValues = new ArrayList<>();

		public String toString() {
			String toReturn = this.name+'\n';
			toReturn+=headers.toString()+'\n';
			for (List<String> entry:dataValues) {
				toReturn+=entry.toString()+'\n';
			}
			return toReturn;
		}

		public boolean hasHeaders() {
			return headers!=null;
		}

		public String getName() {
			return name;
		}

		public List<String> getHeaders() {
			return headers;
		}

		public List<List<String>> getData() {
			return dataValues;
		}

		public void setHeaders(List<String> headers) {
			this.headers = headers;
		}

	}
	
	private enum CopperheadFileLineType {
		
		GROUP_DEF("\\s*Group:.*"),
		PROCESS_DEF("\\s*Process:.*"),
		TABLE_DEF("\\s*Table:.*"),
		PARAM_DEF("\\s*Params:.*"),
		TAGS("\\s*@.*"),
		TABLE_ENTRY("\\s*\\|.*"),
		BLANK("\\s*"),
		COMMENT("\\s*#.*"),
		STEP(".*");
		
		private Pattern regex;
		
		CopperheadFileLineType(String regex) {
			this.regex = Pattern.compile(regex);
		}
		
		public static CopperheadFileLineType identifyLine(String line) {
			for (CopperheadFileLineType lineType:CopperheadFileLineType.values()) {
				if (lineType.regex.matcher(line).matches()) {
					return lineType;
				}
			}
			return null;
		}
		
	}
	
}
