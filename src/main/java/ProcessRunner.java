package main.java;

import java.util.ArrayList;
import java.util.List;

import main.test.BaseTest;

public class ProcessRunner {

	public static List<CopperheadFileParser.CopperheadGroup> groupLibrary = new ArrayList<>();
	public static StepProcessor stepProcessor = new StepProcessor(CopperheadRunner.stepDirectory);

	public static void runProcess(CopperheadFileParser.CopperheadProcess cp) {
		if (cp.hasParamSource()) {
			CopperheadFileParser.CopperheadTable paramTable = findCopperheadTable(cp.getParamSource());
			runProcess(cp, paramTable);
		} else {
			BaseTest.beforeProcess();
			List<String> steps = cp.getSteps();
			for (int i = 0; i < steps.size(); i++) {
				stepProcessor.runStep(steps.get(i));
			}
			BaseTest.afterProcess();
		}
	}

	public static void runNakedProcess(CopperheadFileParser.CopperheadProcess cp) {
		List<String> steps = cp.getSteps();
		for (int i=0;i<steps.size();i++) {
			stepProcessor.runStep(steps.get(i));
		}
	}

	public static void runProcess(CopperheadFileParser.CopperheadProcess cp, CopperheadFileParser.CopperheadTable ct) {
		for (List<String> paramSet:ct.getData()) {
			BaseTest.beforeProcess();
			List<String> steps = cp.getSteps();
			for (int i=0;i<steps.size();i++) {
				String parametrizedStep = injectParamsToStep(steps.get(i),ct.getHeaders(),paramSet);
				stepProcessor.runStep(parametrizedStep);
			}
			BaseTest.afterProcess();
		}
	}

	public static CopperheadFileParser.CopperheadTable findCopperheadTable(String tableName) {
		for (CopperheadFileParser.CopperheadGroup cg:groupLibrary) {
			for (CopperheadFileParser.CopperheadTable ct:cg.getTables()) {
				if (ct.getName().equals(tableName)) {
					return ct;
				}
			}
		}
		throw new RuntimeException("Could not find table with name: "+tableName);
	}

	public static CopperheadFileParser.CopperheadProcess findProcessEndingInStep(String step) {
		for (CopperheadFileParser.CopperheadGroup cg:groupLibrary) {
			for (CopperheadFileParser.CopperheadProcess cp:cg.getProcesses()) {
				if (cp.getSteps().contains(step)) {
					return cp;
				}
			}
		}
		throw new RuntimeException("Could not find process ending in: "+step);
	}

	private static String injectParamsToStep(String step, List<String> headers, List<String> data) {
		for (int i=0;i<headers.size();i++) {
			String macro = "<"+headers.get(i)+">";
			step = step.replaceAll(macro, data.get(i));
		}
		return step;
	}
	
}
