package main.java;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CopperheadRunner {

    public static String copperheadDirectory = "src/test/resources";
    public static String locatorDirectory = "src/main/resources";
    public static String stepDirectory = "bin/main/test";

    public static void main(String[] args) throws Exception {
        List<String> tags = new ArrayList<>();
        tags.add("@TableTest");
        runTags(tags);
    }

    public static void setCopperheadDirectory(String s) {
        copperheadDirectory = s;
    }

    public static void setLocatorDirectory(String s) {
        locatorDirectory = s;
    }

    public static void setStepDirectory(String s) {
        stepDirectory = s;
    }

    public static void runTags(List<String> tags) throws Exception {
        List<File> files = DirectoryIterator.getFilesInDirectory(new File(CopperheadRunner.copperheadDirectory),".*.ch");
        List<CopperheadFileParser.CopperheadGroup> groups = new ArrayList<>();
        for (File f:files) {
            CopperheadFileParser.CopperheadGroup g = CopperheadFileParser.parseCopperheadFile(f);
            groups.add(g);
            System.out.println(g);
        }
        ProcessRunner.groupLibrary = groups;
        TagRunner.runGroups(groups, tags);
    }

}
