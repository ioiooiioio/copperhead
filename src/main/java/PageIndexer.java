package main.java;

import org.openqa.selenium.By;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class PageIndexer {

    static List<File> locatorFiles = null;

    public static void indexLocatorFiles() {
        locatorFiles = DirectoryIterator.getFilesInDirectory(new File(CopperheadRunner.locatorDirectory),".*.loc");
    }

    public static Properties getFilePropsFromPageName(String pageName) {
        if (locatorFiles==null) {
            indexLocatorFiles();
        }
        for (File locFile:locatorFiles) {
            try {
                Properties fileProps = new Properties();
                fileProps.load(new FileReader(locFile));
                String filePageName = fileProps.getProperty("PAGE");
                if (filePageName.equalsIgnoreCase(pageName)) {
                    return fileProps;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static By getLocatorFromElementAndPage(String elementName, String pageName, String... locatorArgs) {
        elementName = StringFormatter.regToCamelCase(elementName);
        Properties fileProps = getFilePropsFromPageName(pageName);
        if (fileProps==null) {
            System.out.println("Problem finding locator.");
            return null;
        }
        String locatorPattern = fileProps.getProperty(elementName+"XPath");
        if (locatorPattern!=null) {
            return By.xpath(MessageFormat.format(locatorPattern, locatorArgs));
        } else {
            locatorPattern = fileProps.getProperty(elementName);
            return By.cssSelector(MessageFormat.format(locatorPattern, locatorArgs));
        }
    }

    public static void main(String[] args) {
        System.out.println(getLocatorFromElementAndPage("ready button","Gravity Home"));
    }

}
