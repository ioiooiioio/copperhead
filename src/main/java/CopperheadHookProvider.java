package main.java;

public abstract class CopperheadHookProvider {
	
	public static void beforeSuite() {}
	
	public static void afterSuite() {}
	
	public static void beforeProcess() {}
	
	public static void afterProcess() {}
	
}
