package main.java;

import java.util.List;

import main.java.CopperheadFileParser.CopperheadGroup;
import main.java.CopperheadFileParser.CopperheadProcess;

public class TagRunner {

	public static void runGroups(List<CopperheadGroup> groups, List<String> tags) {
		for (CopperheadGroup group:groups) {
			List<CopperheadProcess> processes = group.getProcesses();
			List<String> groupTags = group.getTags();
			for (CopperheadProcess process:processes) {
				List<String> processTags = process.getTags();
				boolean tagMatch = isTagMatch(groupTags,processTags,tags);
				if (tagMatch) {
					ProcessRunner.runProcess(process);
				}
			}
		}
	}
	
	public static boolean isTagMatch(List<String> groupTags,List<String> processTags, List<String> filterTags) {
		for (String filterTag:filterTags) {
			if (filterTag.charAt(0)=='~') {
				filterTag=filterTag.substring(1);
				for (String groupTag:groupTags) {
					if (groupTag.equals(filterTag)) {
						return false;
					}
				}
				for (String processTag:processTags) {
					if (processTag.equals(filterTag)) {
						return false;
					}
				}
			} else {
				boolean individualTagMatch = false;
				for (String groupTag:groupTags) {
					if (groupTag.equals(filterTag)) {
						individualTagMatch = true;
					}
				}
				for (String processTag:processTags) {
					if (processTag.equals(filterTag)) {
						individualTagMatch = true;
					}
				}
				if (!individualTagMatch) {
					return false;
				}
			}
		}
		return true;
	}
	
}
