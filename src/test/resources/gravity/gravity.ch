
@Gravity
Group: Gravity website

@OpenPage
Process: Visit the gravity website
  Open "https://www.gravityforms.com/demo/"
  Click the "ready button" on the "gravity home" page
  Complete the gravity form
  Verify the text value of the "your name label" element on the "gravity home" page is "Your Name"
  Then the form is filled out

Process: Complete the gravity form
  Enter "Brendan" into the "first name input" field on the "gravity home" page
  Enter "Berger" into the "last name input" field on the "gravity home" page

Process: Clear the gravity form
  Clear the "first name input" field on the "gravity home" page
  Clear the "last name input" field on the "gravity home" page

@GivenThenTest
Process:
  Given the form is filled out
  Clear the gravity form

@TableTest
Process: Print colors
Params: Colors
  Print <Color>

Table: Colors
| Color |
| "Red" |
#| "Yellow" |
| "Green" |
